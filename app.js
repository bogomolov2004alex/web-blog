import flash from 'connect-flash';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import express from 'express';
import expressLayouts from 'express-ejs-layouts';
import fileUpload from 'express-fileupload';
import session from 'express-session';
import mongoose from 'mongoose';
import passport from 'passport';
import path from 'path';
import { fileURLToPath } from 'url';

import { options, uri } from './db.js';
import router from './server/route/index.js';

import './server/strategy/localStrategy.js';

dotenv.config();

const port = process.env.PORT || 3000;
const secret = process.env.SECRET || 'secret';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();

/** Middlewares: */
app.use(expressLayouts);
app.use(cookieParser('FoodBlogSecure'));
app.use(
	session({
		secret: secret,
		saveUninitialized: false,
		resave: false,
		cookie: {
			maxAge: 60000 * 60,
		},
	})
);
app.use(flash());
app.use(fileUpload());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.resolve(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

/** Routes:
 * 1. /api/v1 - homepage
 * 2. * - 404
 */
app.use('/api/v1', router);

app.get('*', (req, res) => {
	res.status(404).render('404', {
		title: 'Blog: 404 Page Not Found',
		layout: './layout/main.ejs',
		user: req.user,
	});
});

app.set('view engine', 'ejs');
app.set('layout', './layout/main');
app.set('views', './view');

async function start() {
	try {
		await mongoose.connect(uri, options);

		console.log('Connected correctly to MongoDB');

		app.listen(port, err => {
			if (err) return console.error(err);
			console.log(`Server is running on port ${port}`);
		});
	} catch (err) {
		console.error(err);
	}
}

start().then(_ =>
	console.log('Connections has been established successfully.')
);
