import bcrypt from 'bcrypt';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import UserModel from './server/model/User.js';

const setupLocalStrategy = () => {
	passport.use(
		new LocalStrategy(async (email, password, done) => {
			try {
				// Find the user by username in the database
				const user = await UserModel.findOne({ email });
				// If the user does not exist, return an error
				if (!user) {
					return done(null, false, { error: 'Incorrect email' });
				}

				// Compare the provided password with the
				// hashed password in the database
				const passwordsMatch = await bcrypt.compare(password, user.password);

				// If the passwords match, return the user object
				if (passwordsMatch) {
					return done(null, user);
				} else {
					// If the passwords don't match, return an error
					return done(null, false, { error: 'Incorrect password' });
				}
			} catch (err) {
				return done(err);
			}
		})
	);
};

export default setupLocalStrategy;
