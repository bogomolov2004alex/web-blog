const onUrlChange = currentPath => {
	switch (currentPath) {
		case '':
		case 'v1':
			console.log('Homepage page');

			const infoBlock = document.getElementById('infoBlock');

			document.getElementById('infoBtn').addEventListener('click', () => {
				infoBlock.hidden = !infoBlock.hidden;
			});

			const dropdown = document.getElementById('dropdown');

			dropdown.addEventListener('click', () => {
				const filterHeader = document.getElementById('filterHeader');

				const filterOldest = document.getElementById('filterOldest');
				const filterNewest = document.getElementById('filterNewest');
				const filterAsc = document.getElementById('filterAsc');
				const filterDesc = document.getElementById('filterDesc');

				const filteredOldestRecipesBlock = document.getElementById('oldest');
				const filteredNewestRecipesBlock = document.getElementById('newest');
				const filteredAscRecipesBlock = document.getElementById('asc');
				const filteredDescRecipesBlock = document.getElementById('desc');

				const showFilteredBlock = filterType => {
					document.getElementById('default').style.display = 'none';

					filteredOldestRecipesBlock.style.display = 'none';
					filteredNewestRecipesBlock.style.display = 'none';
					filteredAscRecipesBlock.style.display = 'none';
					filteredDescRecipesBlock.style.display = 'none';

					switch (filterType) {
						case 'oldest':
							filteredOldestRecipesBlock.removeAttribute('style');
							break;
						case 'newest':
							filteredNewestRecipesBlock.removeAttribute('style');
							break;
						case 'asc':
							filteredAscRecipesBlock.removeAttribute('style');
							break;
						case 'desc':
							filteredDescRecipesBlock.removeAttribute('style');
							break;
						default:
							document.getElementById('default').removeAttribute('style');
							break;
					}
				};

				filterOldest.addEventListener('click', () => {
					console.log('Oldest');

					filterHeader.innerHTML = filterOldest.innerHTML;

					showFilteredBlock('oldest');
				});

				filterNewest.addEventListener('click', () => {
					console.log('Newest');

					filterHeader.innerHTML = filterNewest.innerHTML;

					showFilteredBlock('newest');
				});

				filterAsc.addEventListener('click', () => {
					console.log('Asc');

					filterHeader.innerHTML = 'Ascending';

					showFilteredBlock('asc');
				});

				filterDesc.addEventListener('click', () => {
					console.log('Desc');

					filterHeader.innerHTML = 'Descending';

					showFilteredBlock('desc');
				});
			});

			break;
		case 'contact':
			console.log('Contact page');

			const nameField = document.getElementById('name');
			const emailField = document.getElementById('email');
			const messageField = document.getElementById('message');
			const submitButton = document.getElementById('submitButton');

			nameField.addEventListener('input', () => {
				if (
					emailField.value.trim().length > 0 &&
					messageField.value.trim().length > 0
				) {
					submitButton.disabled = false;
				}
			});

			emailField.addEventListener('input', () => {
				if (
					nameField.value.trim().length > 0 &&
					messageField.value.trim().length > 0
				) {
					submitButton.disabled = false;
				}
			});

			messageField.addEventListener('input', () => {
				if (
					emailField.value.trim().length > 0 &&
					nameField.value.trim().length > 0
				) {
					submitButton.disabled = false;
				}
			});
			break;
		case 'submit-page':
			console.log('Submit page');

			const addIngredientsBtn = document.getElementById('addIngredientsBtn');
			const removeIngredientsBtn = document.getElementById(
				'removeIngredientsBtn'
			);
			const ingredientList = document.querySelector('.ingredientList');
			const ingredientDiv = document.querySelectorAll('.ingredientDiv')[0];
			const warningDiv = document.querySelector('.warningDiv');

			addIngredientsBtn.addEventListener('click', () => {
				const newIngredient = ingredientDiv.cloneNode(true);
				const input = newIngredient.getElementsByTagName('input')[0];

				warningDiv.style.display = 'none';
				input.value = '';
				ingredientList.appendChild(newIngredient);
			});

			removeIngredientsBtn.addEventListener('click', () => {
				if (ingredientList.children.length > 1) {
					warningDiv.style.display = 'none';
					ingredientList.removeChild(ingredientList.lastChild);
				} else {
					warningDiv.style.display = 'flex';
				}
			});
			break;
		case 'registration':
			console.log('Registration page');

			document.getElementById('login-layout').style.display = 'none';
			document.getElementById('reg-layout').style.display = 'block';

			const firstName = document.getElementById('firstName');
			const email = document.getElementById('reg-email');

			const registerButton = document.getElementById('registerButton');
			const passwordMismatchMessage = document.getElementById(
				'passwordMismatchMessage'
			);
			const password = document.getElementById('reg-password');
			const passwordConfirmation = document.getElementById(
				'passwordConfirmation'
			);

			const invalidChars = /[#$%^&*()!+=/:;<=>?@,.?'"{}]/;
			const minPasswordLength = 6;

			passwordConfirmation.disabled = true;
			registerButton.disabled = true;

			let isValidPasswords = false;

			const validatePasswords = () => {
				if (password.value.length === 0) {
					passwordConfirmation.value = '';
					passwordConfirmation.disabled = true;
					passwordMismatchMessage.style.display = 'none';
					return;
				}

				passwordConfirmation.disabled = false;

				if (
					password.value.trim().length <= 0 ||
					invalidChars.test(password.value)
				) {
					passwordMismatchMessage.innerText =
						'Incorrect password. Password shouldn`t contain symbols';
					passwordMismatchMessage.style.display = 'block';
					return;
				}

				if (password.value.length < minPasswordLength) {
					passwordMismatchMessage.innerText = `Password must be at least ${minPasswordLength} characters`;
					passwordMismatchMessage.style.display = 'block';
					return;
				}

				if (
					passwordConfirmation.value.length > 0 &&
					password.value !== passwordConfirmation.value
				) {
					isValidPasswords = false;
					// registerButton.disabled = true;
					passwordMismatchMessage.innerText = 'Passwords mismatch';
					passwordMismatchMessage.style.display = 'block';
					return;
				}

				if (passwordConfirmation.value.length === 0) {
					passwordMismatchMessage.style.display = 'none';
					return;
				}

				isValidPasswords = true;
				console.log(isValidPasswords);
				// registerButton.disabled = false;
				passwordMismatchMessage.style.display = 'none';
			};

			firstName.addEventListener('input', () => {
				if (email.value.trim().length > 0 && isValidPasswords) {
					registerButton.disabled = false;
				}
			});

			email.addEventListener('input', () => {
				if (firstName.value.trim().length > 0 && isValidPasswords) {
					registerButton.disabled = false;
				}
			});

			password.addEventListener('input', () => {
				validatePasswords();
			});

			passwordConfirmation.addEventListener('input', () => {
				validatePasswords();
			});

			document.getElementById('toLoginBtn').addEventListener('click', () => {
				console.log('toLoginBtn');
				if (window.history.replaceState) {
					//prevents browser from storing history with each change:
					window.history.replaceState(null, '', '/api/v1/users/login');
					onUrlChange('login');
				}
			});

			break;
		case 'login':
			console.log('Login page');

			document.getElementById('reg-layout').style.display = 'none';
			document.getElementById('login-layout').style.display = 'block';

			document
				.getElementById('toRegistrationBtn')
				.addEventListener('click', () => {
					console.log('toRegisterBtn');
					if (window.history.replaceState) {
						//prevents browser from storing history with each change:
						window.history.replaceState(null, '', '/api/v1/users/registration');
						onUrlChange('registration');
					}
				});
			break;
		case 'admin':
			console.log('Admin page');

			const adminAction = document.getElementById('adminAction');
			const addSection = document.getElementById('addSection');
			const editSection = document.getElementById('editSection');
			const deleteSection = document.getElementById('deleteSection');

			adminAction.addEventListener('change', () => {
				hideAllSections();

				switch (adminAction.value) {
					case 'add':
						addSection.style.display = 'block';
						break;
					case 'edit':
						editSection.style.display = 'block';
						loadRecipesForEditing();
						break;
					case 'delete':
						deleteSection.style.display = 'block';
						loadRecipesForDeletion();
						break;
				}
			});

			document.getElementById('addForm').addEventListener('submit', e => {
				e.preventDefault();
				// Add recipe to database logic here
				alert('Recipe added!');
			});

			const hideAllSections = () => {
				addSection.style.display = 'none';
				editSection.style.display = 'none';
				deleteSection.style.display = 'none';
			};

			const loadRecipesForEditing = () => {
				// Load recipes from the database and display them for editing
				const editRecipesList = document.getElementById('editRecipesList');
				editRecipesList.innerHTML = '<p>Loading recipes for editing...</p>';
				// Replace the above line with actual logic to fetch and display recipes
			};

			const loadRecipesForDeletion = () => {
				// Load recipes from the database and display them for deletion
				const deleteRecipesList = document.getElementById('deleteRecipesList');
				deleteRecipesList.innerHTML = '<p>Loading recipes for deletion...</p>';
				// Replace the above line with actual logic to fetch and display recipes
			};
			break;
		default:
			break;
	}
};

window.onload = () => {
	const currentPage = window.location.pathname;

	onUrlChange(currentPage.match(/[^\/]*$/)[0]);
};
