import CategoryModel from '../model/Category.js';
import RecipeModel from '../model/Recipe.js';

class CategoryController {
	/**
	 * GET /categories
	 * Explore categories
	 */
	async getCategories(req, res) {
		try {
			const limitNumberOfCategories = 20;
			const categories = await CategoryModel.find().limit(
				limitNumberOfCategories
			);

			res.render('categories', {
				title: 'Blog: Categories',
				categories,
				user: req.user,
			});
		} catch (err) {
			res.status(500).send({ message: err.message || 'Error occurred' });
		}
	}

	/**
	 * GET /categories/:slug
	 * Categories By Slug
	 */
	async getCategoryBySlug(req, res) {
		try {
			const limitNumber = 20;
			const categorySlug = req.params.slug;

			const category = await CategoryModel.findOne({ slug: categorySlug });
			const categoryName = category.name;
			const recipes = await RecipeModel.find({ category: categoryName }).limit(
				limitNumber
			);

			res.render('categories', {
				title: 'Blog: Categories',
				categoryName,
				recipes,
				user: req.user,
			});
		} catch (error) {
			res.status(500).send({ message: error.message || 'Error occurred' });
		}
	}
}

export default new CategoryController();
