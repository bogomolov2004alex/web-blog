import CategoryModel from '../model/Category.js';
import FeedbackModel from '../model/Feedback.js';
import RecipeModel from '../model/Recipe.js';
import RecipeController from './RecipeController.js';

class MainController {
	/**
	 * GET /
	 * Homepage
	 */
	async homepage(req, res) {
		try {
			const recipeController = RecipeController;
			const limitNumber = 5;

			const categories = await CategoryModel.find().limit(limitNumber);

			const updated = await recipeController.getSortedRecipes(
				'default',
				limitNumber
			);

			if (updated == null) {
				throw new Error(
					"Couldn't find a recipe with the specified sort type or limit number"
				);
			}

			const thai = await RecipeModel.find({ category: 'Thai' }).limit(
				limitNumber
			);
			const american = await RecipeModel.find({ category: 'American' }).limit(
				limitNumber
			);
			const chinese = await RecipeModel.find({ category: 'Chinese' }).limit(
				limitNumber
			);

			const recipes = { updated, thai, american, chinese };

			const filteredRecipes = {
				newest: await recipeController.getSortedRecipes('newest', limitNumber),
				oldest: await recipeController.getSortedRecipes('oldest', limitNumber),
				asc: await recipeController.getSortedRecipes('asc', limitNumber),
				desc: await recipeController.getSortedRecipes('desc', limitNumber),
			};

			res.render('index', {
				title: 'Blog: Home',
				categories,
				recipes,
				filteredRecipes,
				limitNumber,
				user: req.user,
			});
		} catch (err) {
			res.status(500).send({ message: err.message || 'Error occurred' });
		}
	}

	/**
	 * POST /search/
	 * Search
	 */
	async searchRecipe(req, res) {
		try {
			const searchTerm = req.body.searchTerm;
			const searchResult = await RecipeModel.find({
				$text: { $search: searchTerm, $diacriticSensitive: true },
			});

			res.render('search', {
				title: 'Blog: Search',
				searchResult,
				searchTerm,
				user: req.user,
			});
		} catch (error) {
			res.status(500).send({ message: error.message || 'Error occurred' });
		}
	}

	/**
	 * GET /contact
	 * Get contact information
	 */
	async contact(req, res) {
		const reqMessage = req.flash('reqMessage')[0];
		const reqError = req.flash('reqError')[0];

		if (reqMessage) console.log(reqMessage.message);

		res.render('contact', {
			title: 'Blog: Contact',
			reqMessage,
			reqError,
			user: req.user,
		});
	}

	/**
	 * POST /feedback
	 * Sending user`s request
	 */
	async feedback(req, res) {
		try {
			const { name, email, message } = req.body;

			const newFeedback = new FeedbackModel({
				name,
				email,
				message,
			});
			await newFeedback.save();

			req.flash('reqMessage', { message: 'Заявка принята.' });
			res.redirect('/api/v1/contact');
		} catch (err) {
			req.flash('reqError', err.message);
			res.status(500).redirect('/api/v1/contact');
		}
	}

	/**
	 * GET /explore-latest
	 * Explore Latest
	 */
	async exploreLatest(req, res) {
		try {
			const limitNumber = 20;
			const latestRecipes = await RecipeModel.find({})
				.sort({ updatedAt: -1 })
				.limit(limitNumber);

			res.render('explore-latest', {
				title: 'Blog: Explore Latest',
				latestRecipes,
				user: req.user,
			});
		} catch (error) {
			res.status(500).send({ message: error.message || 'Error occurred' });
		}
	}

	/**
	 * GET /explore-random
	 * Explore Random as JSON
	 */
	async exploreRandom(req, res) {
		try {
			const count = await RecipeModel.find().countDocuments();
			const random = Math.floor(Math.random() * count);
			const randomRecipe = await RecipeModel.findOne().skip(random).exec();

			res.render('explore-random', {
				title: 'Blog: Explore Latest',
				randomRecipe,
				user: req.user,
			});
		} catch (error) {
			res.status(500).send({ message: error.message || 'Error occurred' });
		}
	}

	/**
	 * GET /submit-page
	 * Submit Recipe
	 */
	async getSubmitRecipeForm(req, res) {
		let categories;

		try {
			categories = await CategoryModel.find({});
		} catch (err) {
			req.flash('infoErrors', err);
		}

		const infoErrorsObj = req.flash('infoErrors');
		const infoSubmitObj = req.flash('infoSubmit');

		res.render('submit-page', {
			title: 'Blog: Submit Recipe',
			infoErrorsObj,
			infoSubmitObj,
			categories,
			user: req.user,
		});
	}

	/**
	 * POST /submit-page
	 * Submit Recipe
	 */
	async submitRecipe(req, res) {
		try {
			let imageUploadFile;
			let uploadPath;
			let newImageName;

			if (!req.files || Object.keys(req.files).length === 0) {
				console.log('No Files were uploaded.');
			} else {
				imageUploadFile = req.files.image;
				newImageName = Date.now() + '-' + imageUploadFile.name;

				uploadPath = path.resolve('./') + '/public/uploads/' + newImageName;
			}

			const name = req.body.name;
			const slug = Handler.getSlug(name);
			console.log(slug);

			const newRecipe = new RecipeModel({
				slug,
				name,
				description: req.body.description,
				email: req.user.email,
				ingredients: req.body.ingredients,
				category: req.body.category,
				image: newImageName,
			});

			await newRecipe.save();

			if (uploadPath) {
				await imageUploadFile.mv(uploadPath, err => {
					if (err) return res.status(500).send(err);
				});
			}

			req.flash('infoSubmit', 'Recipe has been added.');
			res.redirect('/api/v1/submit-page');
		} catch (err) {
			req.flash('infoErrors', err);
			res.redirect('/api/v1/submit-page');
		}
	}

	/**
	 * GET error
	 * Any error page render
	 */
	// async error(err, req, res, next) {
	// 	res.status(err.status || 500).render('error', {
	// 		title: 'Blog: Internal Server Error',
	// 		message: err.message || 'Internal Server Error',
	// 		layout: './layout/main.ejs',
	//    user: req.user,
	// 	});
	// }

	async insertDummyRecipeData(req, res) {
		try {
			const recipes = [
				{
					slug: 'bbq-chicken',
					name: 'BBQ chicken',
					description: `Preheat the oven to 190°C/375°F/gas 5. Place the ketchup, honey, Worcestershire sauce, most of the smoked paprika and 1 tablespoon of red wine vinegar in a snug-fitting roasting tray and mix together. Add the chicken and toss to coat. At this point, you can either leave the tray covered in the fridge to marinate, or season with a pinch of sea salt and black pepper and set aside while you prep the potatoes.
	        Scrub and quarter the potatoes lengthways and place in another large roasting tray. Drizzle in 2 tablespoons of olive oil, season and sprinkle over a pinch of smoked paprika, tossing until coated. Arrange the potatoes skin-side down in one even layer. Place the potato tray on the middle shelf of the oven and the chicken tray on the top shelf to roast for 20 minutes.
	        Once the time’s up, carefully spoon off any fat from the chicken and add to the potatoes, tossing to coat. Baste the chicken in the remaining juices, scraping up any sticky bits from the bottom of the tray. Return both trays to the oven to roast for a further 25 minutes or until the chicken pulls away easily from the bone, placing the frozen corn straight onto the bars of the bottom shelf to cook alongside.
	
					For more: https://www.jamieoliver.com/recipes/chicken-recipes/bbq-chicken/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'75 ml tomato ketchup',
						'2 tablespoons runny honey',
						'1 tablespoon Worcestershire sauce',
						'1 teaspoon smoked paprika',
						'red wine vinegar',
						'6 chicken thighs , bone in, skin on',
					],
					category: 'American',
					image: 'bbq-chicken.jpg',
				},
				{
					slug: 'beautiful-crab-cakes',
					name: 'Beautiful crab cakes',
					description: `Add the potatoes to a large pan of salted boiling water and cook for 15 to 20 minutes, or until tender. Meanwhile, blacken the pepper, tomatoes and chilli in a large frying pan on a high heat with a small splash of olive oil, then leave to cool.
	        Drain the potatoes in a colander and leave to steam dry for a few minutes then return to the pan. Add a pinch of salt and pepper and the crabmeat then mash with a potato masher.
	        Finely grate the zest of a lemon onto a board, add the parsley, chilli and spring onions and chop it all together, mixing as you go, until fine.
	
					For more: https://www.jamieoliver.com/recipes/seafood-recipes/beautiful-homemade-crab-cakes-with-a-hot-blackened-salsa/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'500 g potatoes , peeled and larger ones halved',
						'sea salt',
						'freshly ground black pepper',
						'olive oil',
						'500 g mixed white and brown crabmeat , 2:1 brown to white, from sustainable sources, ask your fishmonger',
					],
					category: 'American',
					image: 'beautiful-crab-cakes.jpg',
				},
				{
					slug: 'chocolate-banoffee-whoopie-pies',
					name: 'Chocolate & banoffee whoopie pies',
					description: `Preheat the oven to 170ºC/325ºF/gas 3 and line 2 baking sheets with greaseproof paper.
					Combine the cocoa powder with a little warm water to form a paste, then add to a bowl with the remaining whoopie ingredients. Mix into a smooth, slightly stiff batter.
					Spoon equal-sized blobs, 2cm apart, onto the baking sheets, then place in the hot oven for 8 minutes, or until risen and cooked through.
	
					For more: https://www.jamieoliver.com/recipes/chocolate-recipes/chocolate-amp-banoffee-whoopie-pies/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'2 heaped tablespoons cocoa powder, plus extra for dusting',
						'350 g self-raising flour',
						'175 g sugar',
						'200 ml milk',
						'100 ml nut or rapeseed oil',
						'1 large free-range egg',
					],
					category: 'American',
					image: 'chocolate-banoffe-whoopie-pies.jpg',
				},
				{
					slug: 'crab-cakes',
					name: 'Crab cakes',
					description: `Trim and finely chop the spring onions, and pick and finely chop the parsley. Beat the egg.
					Combine the crabmeat, potatoes, spring onion, parsley, white pepper, cayenne and egg in a bowl with a little sea salt.
					Refrigerate for 30 minutes, then shape into 6cm cakes.
	
					For more: https://www.jamieoliver.com/recipes/seafood-recipes/crab-cakes/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'3 spring onions',
						'½ a bunch of fresh flat-leaf parsley',
						'1 large free-range egg',
						'750 g cooked crabmeat , from sustainable sources',
						'300 g mashed potatoes',
					],
					category: 'American',
					image: 'crab-cakes.jpg',
				},
				{
					slug: 'key-lime-pie',
					name: 'Key lime pie',
					description: `Preheat the oven to 175ºC/gas 3. Lightly grease a 22cm metal or glass pie dish with a little of the butter.
					For the pie crust, blend the biscuits, sugar and remaining butter in a food processor until the mixture resembles breadcrumbs.
					Transfer to the pie dish and spread over the bottom and up the sides, firmly pressing down.
	
					For more: https://www.jamieoliver.com/recipes/fruit-recipes/key-lime-pie/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'4 large free-range egg yolks',
						'400 ml condensed milk',
						'5 limes',
						'200 ml double cream',
					],
					category: 'American',
					image: 'key-lime-pie.jpg',
				},
				{
					slug: 'grilled-lobster-rolls',
					name: 'Grilled lobster rolls',

					description: `Remove the butter from the fridge and allow to soften.
					Preheat a griddle pan until really hot.
					Butter the rolls on both sides and grill on both sides until toasted and lightly charred (keep an eye on them so they don’t burn).
	
					For more: https://www.jamieoliver.com/recipes/seafood-recipes/grilled-lobster-rolls/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'85 g butter',
						'6 submarine rolls',
						'500 g cooked lobster meat, from sustainable sources',
						'1 stick of celery',
						'2 tablespoons mayonnaise , made using free-range eggs',
					],
					category: 'American',
					image: 'grilled-lobster-rolls.jpg',
				},
				{
					slug: 'veggie-pad-thai',
					name: 'Veggie pad thai',
					description: `Cook the noodles according to the packet instructions, then drain and refresh under cold running water and toss with 1 teaspoon of sesame oil.
					Lightly toast the peanuts in a large non-stick frying pan on a medium heat until golden, then bash in a pestle and mortar until fine, and tip into a bowl.
					Peel the garlic and bash to a paste with the tofu, add 1 teaspoon of sesame oil, 1 tablespoon of soy, the tamarind paste and chilli sauce, then squeeze and muddle in half the lime juice.
	
					For more: https://www.jamieoliver.com/recipes/vegetable-recipes/veggie-pad-thai/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'150 g rice noodles',
						'sesame oil',
						'20 g unsalted peanuts',
						'2 cloves of garlic',
						'80 g silken tofu',
						'low-salt soy sauce',
					],
					category: 'Thai',
					image: 'veggie-pad-thai.jpg',
				},
				{
					slug: 'thai-red-chicken-soup',
					name: 'Thai red chicken soup',
					description: `Sit the chicken in a large, deep pan.
					Carefully halve the squash lengthways, then cut into 3cm chunks, discarding the seeds.
					Slice the coriander stalks, add to the pan with the squash, curry paste and coconut milk, then pour in 1 litre of water. Cover and simmer on a medium heat for 1 hour 20 minutes.
	
					For more: https://www.jamieoliver.com/recipes/chicken-recipes/thai-red-chicken-soup/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'1 x 1.6 kg whole chicken',
						'1 butternut squash (1.2kg)',
						'1 bunch of fresh coriander (30g)',
						'100 g Thai red curry paste',
						'1 x 400 ml tin of light coconut milk',
					],
					category: 'Thai',
					image: 'thai-red-chicken-soup.jpg',
				},
				{
					slug: 'thai-green-curry',

					name: 'Thai green curry',
					description: `Preheat the oven to 180ºC/350ºF/gas 4.
					Wash the squash, carefully cut it in half lengthways and remove the seeds, then cut into wedges. In a roasting tray, toss with 1 tablespoon of groundnut oil and a pinch of sea salt and black pepper, then roast for around 1 hour, or until tender and golden.
					For the paste, toast the cumin seeds in a dry frying pan for 2 minutes, then tip into a food processor.
	
					For more: https://www.jamieoliver.com/recipes/butternut-squash-recipes/thai-green-curry/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'1 butternut squash (1.2kg)',
						'groundnut oil',
						'2x 400 g tins of light coconut milk',
						'400 g leftover cooked greens, such as Brussels sprouts, Brussels tops, kale, cabbage, broccoli',
						'350 g firm silken tofu',
					],
					category: 'Thai',
					image: 'thai-green-curry.jpg',
				},
				{
					slug: 'thai-style-mussels',

					name: 'Thai-style mussels',
					description: `Wash the mussels thoroughly, discarding any that aren’t tightly closed.
					Trim and finely slice the spring onions, peel and finely slice the garlic. Pick and set aside the coriander leaves, then finely chop the stalks. Cut the lemongrass into 4 pieces, then finely slice the chilli.
					In a wide saucepan, heat a little groundnut oil and soften the spring onion, garlic, coriander stalks, lemongrass and most of the red chilli for around 5 minutes.
	
					For more: https://www.jamieoliver.com/recipes/vegetables-recipes/asian-vegetable-broth/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'1 kg mussels , debearded, from sustainable sources',
						'4 spring onions',
						'2 cloves of garlic',
						'½ a bunch of fresh coriander',
						'1 stick of lemongrass',
					],
					category: 'Thai',
					image: 'thai-style-mussels.jpg',
				},
				{
					slug: 'thai-inspired-vegetable-broth',
					name: 'Thai-inspired vegetable broth',
					description: `Peel and crush the garlic, then peel and roughly chop the ginger. Trim the greens, finely shredding the cabbage, if using. Trim and finely slice the spring onions and chilli. Pick the herbs.
					Bash the lemongrass on a chopping board with a rolling pin until it breaks open, then add to a large saucepan along with the garlic, ginger and star anise.
					Place the pan over a high heat, then pour in the vegetable stock. Bring it just to the boil, then turn down very low and gently simmer for 30 minutes.
	
					For more: https://www.jamieoliver.com/recipes/vegetables-recipes/asian-vegetable-broth/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'3 cloves of garlic',
						'5cm piece of ginger',
						'200 g mixed Asian greens , such as baby pak choi, choy sum, Chinese cabbage',
						'2 spring onions',
						'1 fresh red chilli',
					],
					category: 'Thai',
					image: 'thai-inspired-vegetable-broth.jpg',
				},
				{
					slug: 'chinese-steak-tofu-stew',
					name: 'Chinese steak & tofu stew',
					description: `Get your prep done first, for smooth cooking. Chop the steak into 1cm chunks, trimming away and discarding any fat.
					Peel and finely chop the garlic and ginger and slice the chilli. Trim the spring onions, finely slice the top green halves and put aside, then chop the whites into 2cm chunks. Peel the carrots and mooli or radishes, and chop to a similar size.
					Place a large pan on a medium-high heat and toast the Szechuan peppercorns while it heats up. Tip into a pestle and mortar, leaving the pan on the heat.
	
					For more: https://www.jamieoliver.com/recipes/stew-recipes/chinese-steak-tofu-stew/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'250g rump or sirloin steak',
						'2 cloves of garlic',
						'4cm piece of ginger',
						'2 fresh red chilli',
						'1 bunch of spring onions',
						'2 large carrots',
					],
					category: 'American',
					image: 'chinese-steak-tofu-stew.jpg',
				},
				{
					slug: 'thai-chinese-inspired-pinch-salad',
					name: 'Thai-Chinese-inspired pinch salad',
					description: `Peel and very finely chop the ginger and deseed and finely slice the chilli (deseed if you like). Toast the sesame seeds in a dry frying pan until lightly golden, then remove to a bowl.
					Mix the prawns with the five-spice and ginger, finely grate in the lime zest and add a splash of sesame oil. Toss to coat, then leave to marinate.
					Trim the lettuces, discarding any tatty outer leaves, then click the leaves apart. Pick out 12 nice-looking inner leaves (save the remaining lettuce for another recipe).
	
					For more: https://www.jamieoliver.com/recipes/seafood-recipes/asian-pinch-salad/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'5 cm piece of ginger',
						'1 fresh red chilli',
						'25 g sesame seeds',
						'24 raw peeled king prawns , from sustainable sources (defrost first, if using frozen)',
						'1 pinch Chinese five-spice powder',
					],
					category: 'Chinese',
					image: 'thai-chinese-inspired-pinch-salad.jpg',
				},
				{
					slug: 'spring-rolls',
					name: 'Spring rolls',
					description: `Put your mushrooms in a medium-sized bowl, cover with hot water and leave for 10 minutes, or until soft. Meanwhile, place the noodles in a large bowl, cover with boiling water and leave for 1 minute. Drain, rinse under cold water, then set aside.
					For the filling, finely slice the cabbage and peel and julienne the carrot. Add these to a large bowl with the noodles.
					Slice the white part of the spring onions on the diagonal and add to the bowl. Finely slice the green parts into ribbons and reserve for later.
	
					For more: https://www.jamieoliver.com/recipes/vegetables-recipes/stir-fried-vegetables/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'40 g dried Asian mushrooms',
						'50 g vermicelli noodles',
						'200 g Chinese cabbage',
						'1 carrot',
						'3 spring onions',
					],
					category: 'Chinese',
					image: 'spring-rolls.jpg',
				},
				{
					slug: 'stir-fried-vegetables',

					name: 'Stir-fried vegetables',
					description: `Crush the garlic and finely slice the chilli and spring onion. Peel and finely slice the red onion, and mix with the garlic, chilli and spring onion.
					Shred the mangetout, slice the mushrooms and water chestnuts, and mix with the shredded cabbage in a separate bowl to the onion mixture.
					Heat your wok until it’s really hot. Add a splash of oil – it should start to smoke – then the chilli and onion mix. Stir for just 2 seconds before adding the other mix. Flip and toss the vegetables in the wok if you can; if you can’t, don’t worry, just keep the vegetables moving with a wooden spoon, turning them over every few seconds so they all keep feeling the heat of the bottom of the wok. Season with sea salt and black pepper.
	
					For more: https://www.jamieoliver.com/recipes/vegetables-recipes/stir-fried-vegetables/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'1 clove of garlic',
						'1 fresh red chilli',
						'3 spring onions',
						'1 small red onion',
						'1 handful of mangetout',
						'a few shiitake mushrooms',
					],
					category: 'Chinese',
					image: 'stir-fried-vegetables.jpg',
				},
				{
					slug: 'tom-daley-s-sweet-sour-chicken',

					name: "Tom Daley's sweet & sour chicken",
					description: `Drain the juices from the tinned fruit into a bowl, add the soy and fish sauces, then whisk in 1 teaspoon of cornflour until smooth. Chop the pineapple and peaches into bite-sized chunks and put aside.
					Pull off the chicken skin, lay it flat in a large, cold frying pan, place on a low heat and leave for a few minutes to render the fat, turning occasionally. Once golden, remove the crispy skin to a plate, adding a pinch of sea salt and five-spice.
					Meanwhile, slice the chicken into 3cm chunks and place in a bowl with 1 heaped teaspoon of five-spice, a pinch of salt, 1 teaspoon of cornflour and half the lime juice. Peel, finely chop and add 1 clove of garlic, then toss to coat.
	
					For more: https://www.jamieoliver.com/recipes/chicken-recipes/tom-daley-s-sweet-sour-chicken/`,
					email: 'recipeemail@bogomolov.mirea.ru',
					ingredients: [
						'1 x 227 g tin of pineapple in natural juice',
						'1 x 213 g tin of peaches in natural juice',
						'1 tablespoon low-salt soy sauce',
						'1 tablespoon fish sauce',
						'2 teaspoons cornflour',
						'2 x 120 g free-range chicken breasts , skin on',
					],
					category: 'Chinese',
					image: 'tom-daley.jpg',
				},
			];

			for (const recipe of recipes) {
				const createdRecipe = new RecipeModel(recipe);
				await createdRecipe
					.save()
					.then(createdRecipe => {
						console.log(`Recipe created with ID ${createdRecipe._id}`);
						// res.status(200).json(createdCategory);
					})
					.catch(error => {
						console.error('Error creating recipe: ', error.message);
						// res.status(500).json({error: error.message});
					});
			}
			this.homepage(req, res);
		} catch (e) {
			console.log('err' + e);
		}
	}

	async insertDummyCategoryData(req, res) {
		try {
			const categories = [
				{
					slug: 'american-food',
					name: 'American',
					image: 'american-food.jpg',
				},
				{
					slug: 'chinese-food',
					name: 'Chinese',
					image: 'chinese-food.jpg',
				},
				{
					slug: 'mexican-food',
					name: 'Mexican',
					image: 'mexican-food.jpg',
				},
				{
					slug: 'indian-food',
					name: 'Indian',
					image: 'indian-food.jpg',
				},
				{
					slug: 'spanish-food',
					name: 'Spanish',
					image: 'spanish-food.jpg',
				},
				{
					slug: 'thai-food',
					name: 'Thai',
					image: 'thai-food.jpg',
				},
			];

			for (const category of categories) {
				const createdCategory = new CategoryModel(category);
				await createdCategory
					.save()
					.then(createdCategory => {
						console.log(`Category created with ID ${createdCategory._id}`);
						// res.status(200).json(createdCategory);
					})
					.catch(error => {
						console.error('Error creating category: ', error.message);
						// res.status(500).json({error: error.message});
					});
			}
		} catch (e) {
			console.log('err' + e);
		}
	}
}

export default new MainController();
