import bcrypt from 'bcrypt';
import RecipeModel from '../model/Recipe.js';
import UserModel from '../model/User.js';

class UserController {
	/**
	 * GET /registration
	 * Registration page
	 */
	async getRegistrationPage(req, res) {
		const regError = req.flash('regError')[0];

		res.render('auth-page', {
			title: 'Blog: Registration',
			regError,
			layout: './layout/main.ejs',
			user: req.user,
		});
	}

	/**
	 * POST /registration
	 * Registration page
	 */
	async registration(req, res) {
		console.log(req.body);
		const { firstName, lastName, email, password } = req.body;

		try {
			const existingUser = await UserModel.findOne({ email });
			if (existingUser) {
				req.flash('regError', 'User with this email already exists');
				res.redirect('/api/v1/users/login');
			}

			const salt = await bcrypt.genSalt(15);
			const hashedPassword = await bcrypt.hash(password, salt);

			const newUser = new UserModel({
				firstName,
				lastName,
				role: 'COMMON',
				email,
				password: hashedPassword,
			});
			await newUser.save();

			res.redirect('/api/v1/users/login');
		} catch (err) {
			req.flash('regError', err.message);
			res.status(500).redirect('/api/v1/users/login');
		}
	}

	/**
	 * GET /login
	 * Login page
	 */
	async getLoginPage(req, res) {
		res.render('auth-page', {
			title: 'Blog: Login',
			layout: './layout/main.ejs',
			user: req.user,
		});
	}

	/**
	 * POST /login
	 * Login page
	 */
	async login(req, res) {
		console.log('Login');

		res.redirect('/api/v1/');
	}

	/**
	 * GET /logout
	 * Logout page
	 */
	async logout(req, res) {
		req.logout(err => {
			if (err) {
				console.error(err);
				return res.sendStatus(500);
			}

			res.redirect('/api/v1/');
		});
	}

	/**
	 * GET /my-recipes
	 * My recipes page
	 */
	async recipes(req, res) {
		const recipes = await RecipeModel.find({ email: req.user.email });

		res.render('recipes', {
			title: 'Blog: My Recipes',
			recipes,
			user: req.user,
		});
	}
}

export default new UserController();
