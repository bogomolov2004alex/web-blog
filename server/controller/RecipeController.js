import CategoryModel from '../model/Category.js';
import RecipeModel from '../model/Recipe.js';

class RecipeController {
	/**
	 * GET /recipe/:id
	 * Explore recipe
	 */
	// async getRecipeById(req, res) {
	// 	try {
	// 		const recipeId = req.params.id;
	// 		const recipe = await RecipeModel.findById(recipeId);
	//
	// 		res.render('categories', {
	// 			title: 'Blog: Categories',
	// 			recipe
	// 			user: req.user,
	// 		});
	// 	} catch (err) {
	// 		res.status(500).send({ message: err.message || "Error occurred" });
	// 	}
	// }

	/**
	 * GET /recipe/:slug
	 * Explore recipe
	 */
	async getRecipeBySlug(req, res) {
		try {
			const recipeSlug = req.params.slug;
			const recipe = await RecipeModel.findOne({ slug: recipeSlug });
			const category = await CategoryModel.findOne({ name: recipe.category });
			const categorySlug = category.slug;

			res.render('recipe', {
				title: 'Blog: Recipe',
				recipe,
				categorySlug,
				user: req.user,
			});
		} catch (err) {
			res.status(500).send({ message: err.message || 'Error occurred' });
		}
	}

	async getSortedRecipes(sortOrder, limitNumber) {
		console.log('Sort order: ', sortOrder);

		let sortCriteria;

		switch (sortOrder) {
			case 'newest':
				sortCriteria = { updatedAt: -1 };
				break;
			case 'oldest':
				sortCriteria = { updatedAt: 1 };
				break;
			case 'asc':
				sortCriteria = { name: 1 };
				break;
			case 'desc':
				sortCriteria = { name: -1 };
				break;
			default:
				sortCriteria = { updatedAt: -1 };
				break;
		}

		try {
			return await RecipeModel.find().sort(sortCriteria).limit(limitNumber);
		} catch (err) {
			console.error(err);
			return null;
		}
	}
}

export default new RecipeController();
