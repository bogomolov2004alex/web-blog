class AdminController {
	/**
	 * GET /admin
	 * Admin page
	 */
	async adminPage(req, res) {
		res.render('admin-page', {
			title: 'Blog: Admin page',
			user: req.user,
		});
	}
}

export default new AdminController();
