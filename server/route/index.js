import express from 'express';
import MainController from '../controller/MainController.js';
import { checkAuth } from '../util/middleware.js';
import adminRouter from './adminRouter.js';
import categoryRouter from './categoryRouter.js';
import recipeRouter from './recipeRouter.js';
import userRouter from './userRouter.js';

const router = express.Router();

/**
 * Recipe Routes
 */
router.get('/', MainController.homepage);
router.post('/search', checkAuth, MainController.searchRecipe);

router.get('/contact', checkAuth, MainController.contact);
router.post('/feedback', checkAuth, MainController.feedback);

router.get('/explore-latest', checkAuth, MainController.exploreLatest);
router.get('/explore-random', checkAuth, MainController.exploreRandom);

router.get('/submit-page', checkAuth, MainController.getSubmitRecipeForm);
router.post('/submit-page', checkAuth, MainController.submitRecipe);

router.use('/users', userRouter);
router.use('/admin', adminRouter);
router.use('/categories', checkAuth, categoryRouter);
router.use('/recipes', checkAuth, recipeRouter);

// router.get('/insert-sample-data', RecipeController.insertDummyRecipeData);

export default router;
