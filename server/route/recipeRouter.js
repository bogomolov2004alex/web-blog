import express from 'express';
import RecipeController from '../controller/RecipeController.js';

const recipeRouter = express.Router();

/**
 * Recipe Routes
 */
recipeRouter.get('/:slug', RecipeController.getRecipeBySlug);

export default recipeRouter;
