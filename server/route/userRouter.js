import express from 'express';
import passport from 'passport';
import UserController from '../controller/UserController.js';
import { checkAuth } from '../util/middleware.js';

const userRouter = express.Router();

/**
 * User Routes
 */
userRouter.get('/registration', UserController.getRegistrationPage);
userRouter.post('/registration', UserController.registration);
userRouter.get('/login', UserController.getLoginPage);
userRouter.post(
	'/login',
	passport.authenticate('local', {
		successRedirect: '/api/v1/',
		failureRedirect: '/api/v1/users/login',
	}),
	UserController.login
);
userRouter.get('/logout', checkAuth, UserController.logout);

userRouter.get('/my-recipes', checkAuth, UserController.recipes);

export default userRouter;
