import express from 'express';
import CategoryController from '../controller/CategoryController.js';

const categoryRouter = express.Router();

/**
 * Category Routes
 */
categoryRouter.get('/', CategoryController.getCategories);
categoryRouter.get('/:slug', CategoryController.getCategoryBySlug);

export default categoryRouter;
