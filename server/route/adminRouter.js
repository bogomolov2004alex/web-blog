import express from 'express';
import AdminController from '../controller/AdminController.js';

const adminRouter = express.Router();

/**
 * Admin Routes
 */
adminRouter.get('/', AdminController.adminPage);

export default adminRouter;
