import bcrypt from 'bcrypt';
import passport from 'passport';
import { Strategy } from 'passport-local';
import UserModel from '../model/User.js';

/**
 * Serialize and deserialize user objects to maintain user sessions
 */
passport.serializeUser((user, done) => {
	console.log('Serialization: ', user);
	done(null, user._id);
});

passport.deserializeUser(async (id, done) => {
	try {
		console.log('Deserialization: ', id);

		const user = await UserModel.findById(id);

		if (!user) throw new Error('User Not Found');

		done(null, user);
	} catch (err) {
		done(err, null);
	}
});

export default passport.use(
	new Strategy({ usernameField: 'email' }, async (email, password, done) => {
		try {
			const user = await UserModel.findOne({ email });

			if (!user) throw new Error('User Not Found');

			console.log('Authentication: ', user);

			console.log('Long passwords comparing...');

			const passwordsMatch = await bcrypt.compare(password, user.password);

			console.log('Passwords compared');

			if (!passwordsMatch) throw new Error('Invalid Credentials');

			done(null, user);
		} catch (err) {
			done(err, null);
		}
	})
);
