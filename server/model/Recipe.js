import mongoose from 'mongoose';

const recipeSchema = new mongoose.Schema(
	{
		slug: {
			type: String,
			required: true,
			unique: true,
		},
		name: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: true,
		},
		email: {
			type: String,
			required: true,
		},
		ingredients: {
			type: Array,
			required: true,
		},
		category: {
			type: String,
			ref: 'Category',
			// enum: ['Thai', 'American', 'Chinese', 'Mexican', 'Indian', 'Spanish'],
			required: true,
		},
		image: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

recipeSchema.index({ name: 'text', category: 'text' });

export default mongoose.model('Recipe', recipeSchema);
