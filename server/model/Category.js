import mongoose from 'mongoose';

const CategorySchema = new mongoose.Schema(
	{
		slug: {
			type: String,
			required: true,
			unique: true,
		},
		name: {
			type: String,
			required: true,
			unique: true,
		},
		image: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

export default mongoose.model('Category', CategorySchema);
