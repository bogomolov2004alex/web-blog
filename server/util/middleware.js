export const checkAuth = (req, res, next) => {
	if (req.session.passport) return next();

	res.redirect('/api/v1/users/login');
};

export const checkAdminStatus = (req, res, next) => {
	const user = req.user;

	if (user && user.role === 'ADMIN') return next();

	res.status(404).render('404', {
		title: 'Blog: 404 Page Not Found',
		layout: './layout/main.ejs',
		user: req.user,
	});
};
