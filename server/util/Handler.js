export default class Handler {
	static getSlug(name) {
		return name
			.toLowerCase()
			.replace(/\s+/g, '-')
			.replace(/[^\w-]+/g, '');
	}

	// static getNameFromSlug(slug) {
	// 	return slug.replace(/-+/g,'').replace(/^\D+/g, '').replace(/\D+$/g, '');
	// }
}
