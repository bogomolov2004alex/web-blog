import dotenv from 'dotenv';

dotenv.config();
/**
 * @type {string}
 */
export const uri = `mongodb://
	${process.env.MONGO_HOST}:
	${process.env.MONGO_PORT}/
	${process.env.MONGO_DB}`;

/**
 * @type {{retryWrites: boolean, appName: string, w: string}}
 */
export const options = {
	retryWrites: true,
	w: 'majority',
	appName: 'Cluster0',
};
